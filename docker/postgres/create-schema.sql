CREATE TABLE IF NOT EXISTS counts
(
    id            bigint NOT NULL,
    email_address text   NOT NULL,
    count         int    NOT NULL,

    CONSTRAINT counts_pkey PRIMARY KEY (id),
    CONSTRAINT counts_email_address_key UNIQUE (email_address)
);
