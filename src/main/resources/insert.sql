-- Example of pgbench command
-- `pgbench -h localhost -U postgres -d postgres -c 10 -j 10 -t 5 -f src/main/resources/insert.sql`
INSERT INTO counts (id, email_address, count) VALUES (1, 'user-001@example.com', 1) ON CONFLICT (id) DO UPDATE SET count = counts.count + 1;
